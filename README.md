tim_site5
=========

[Pelican][1] theme, used for [timburbank.com][2]

[1]: https://getpelican.com/
[2]: https://www.timburbank.com


Thumbnails and Header Images
----------------------------

The index page shows a list of posts with thumbnail images. The thumbnails must be given in the `thumb` attribute in the post's header. Thumbnail images should be 400x225

If given, a banner image can be shown at the top of a post. It must be given with the `image` attribute. Optionally an `image_alt` attribute can specify alt text for the banner image. Banner images should be 900x240

The utility script `utils/resize_images.py` helps save images in the correct sizes.

A standard post header would look like:

    ---
    layout: post
    title:  Science on a Sphere: Exploring the Unknown Ocean
    slug: sos
    date:   2017-05-01 00:00:00 +0400
    image: /images/sos_dataset_image_header.jpg
    image_alt: Multibeaming sonar illustration on The Sphere
    thumb: /images/sos_dataset_image_thumb.jpg
    ---


Category Pages
--------------

If a page is given a `category` in it's header, it will show any posts with a `tags` that matches the page category. See [timburbank.com/the-robots-heart](https://www.timburbank.com/the-robots-heart/)



Social Links
------------

The Pelican config file can contain a SOCIALS definition, with a tuple of social media URLs

    SOCIAL = (
    	('Twitter', 'https://twitter.com/timburbank'),
        ('Mastodon', 'https://squid.cafe/@robotfriend'))

These are rendered with SVG icons, based on the URL. (Except for the Mastodon icon, which will render if the name column is "Mastodon", since Mastodon instances don't have predictable URLs)


External Link Card
------------------

`templates/external_link.html` contains an HTML snippet that an be pasted
into the markdown of a post's content to show a card for a link to external
content. See 
[timburbank.com/forever-alive/](https://www.timburbank.com/forever-alive/)