/*
<audio> tag can't seek in chrome, maybe due to missing 
Content-Length, Content-Range, Content-Type headers
(either provided by webserver, or in file metadata?)

Also I think current version awkardly loads all audio files at once

Maybe worth using library like howler.js to handle these edge cases
Or maybe I need to learn to server audio right?
*/

var playButton = document.getElementById("play");
var pauseButton = document.getElementById("pause");
var nextButton = document.getElementById("next-button");
var prevButton = document.getElementById("prev-button");
var timeline = document.getElementById("timeline-control");
var timelineProgress = document.getElementById("timeline-progress");
var timelineProgressOverlay = document.getElementById("timeline-progress-overlay");
var timelineUnProgressOverlay = document.getElementById("timeline-unprogress-overlay");
var progressBarFilled = document.getElementById("progress-filled");
var nowPlayingCover = document.getElementById("now-playing-cover");
var downloadLinks = document.getElementById("download");
var loadingIndicator = document.getElementById("loading")
var duration;
var songs = document.getElementsByClassName("song");
var activeSong = songs[0];

var sound = new Howl({
  src: [activeSong.getElementsByTagName("a")[0].href]
});

function setPlayHead(progressAmount) {
	/*
	ProgressAmount is a value from 0-1
	*/
	var progressPercent = progressAmount * 100;
	var remainingPercent = (1 - progressAmount) * 100;

	overlayCss = "right:" + remainingPercent + "%;"
	timelineProgressOverlay.style.cssText = overlayCss;

	unProgressOverlayCss = "left:" + progressPercent + "%;"
	timelineUnProgressOverlay.style.cssText = unProgressOverlayCss;	

	progressCss = "clip-path: inset(0 " + remainingPercent + "% 0 0);"
	timelineProgress.style.cssText = progressCss;

	progressBarFilled.style.width = progressPercent + "%";
}


// function timeUpdate() {

// }


function seekToClick(clickEvent) {
	//// offsetX is listed as experimental, but behaves 
	//// consistently between browsers
	var xClickPos = clickEvent.offsetX;
	var timelineWidth = timeline.offsetWidth;
	var targetTime = (xClickPos / timelineWidth) * duration;

	sound.seek(targetTime);
	window.requestAnimationFrame(playheadAnim);
}

function seekToOverlayClick(clickEvent) {
	/* 
	Timeline overlay is a gradient effect that follows
	the playhead and prevents us from clicking on the timeline.
	*/
	
	//// offsetX is listed as experimental, but behaves consistently 
	//// between browsers
	var xClickPos = clickEvent.offsetX;
	var timelineWidth = timeline.offsetWidth;
	var clickTimeBeforePlayhead = (1 - (xClickPos / timelineWidth)) * duration;
	var targetTime = sound.seek() - clickTimeBeforePlayhead;

	sound.seek(targetTime);
	window.requestAnimationFrame(playheadAnim);
}


function resetSong(audio) {
	sound.stop()
	setPlayHead(0);
	playButton.style.visibility = "visible";
	pauseButton.style.visibility = "hidden";
}


function getContainerLi(domObject) {
	if (domObject.tagName == "LI") {
		return domObject;
	}
	else {
		return getContainerLi(domObject.parentElement);
	}
}


function appendDownloadLink(path) {
		var sourceLink = document.createElement('a');
		sourceLink.href = path;
		sourceLink.innerHTML = path.split(".").pop();
		downloadLinks.appendChild(sourceLink);

		var sourceListItem = document.createElement('li');
		sourceListItem.appendChild(sourceLink);
		downloadLinks.appendChild(sourceListItem);
}

function selectSong(songData) {
	/*
	songData is a container (usually li) element with all the song data tags
	*/
	console.log("select song");
	console.log(songData);

	resetSong(sound);

	try {
		//// Show new song active in song list
		activeSong.setAttribute("class", "song");
	} catch (error) {
		console.error(error);
	}

	activeSong = songData;
	
	activeSong.setAttribute("class", "song active");
	downloadLinks.innerHTML = "";

	var sourceLinks = activeSong.getElementsByTagName("a");
	var sources = [];
	for (let i = 0; i < sourceLinks.length; i++) {
		sources.push(sourceLinks[i].href);
		appendDownloadLink(sourceLinks[i].href);
	}

	if (sources.length == 0) {
		console.warn("No sources");
	}

	duration = 0;
	sound =  new Howl({
		src: sources,
		onplay: function () {
			window.requestAnimationFrame(playheadAnim);
		},
		onend: nextSongPlay,
		onload: function () {
			duration = sound.duration();
		},
		onloaderror: function(id, err) {
      console.warn('failed to load sound file:' + id + err);
    }
    // html5: true //// loads faster but causes weird behaviour in Chrome
	});


	//// Update interface things

	var coverUrl = songData.getElementsByClassName("cover")[0].src;
	nowPlayingCover.src = coverUrl;


	var timelineUrl = songData.getElementsByClassName("timeline")[0].src;
	timeline.src = timelineUrl;
	timelineProgress.getElementsByTagName("img")[0].src = timelineUrl;

	// most of these are delcared at beginning, but maybe this better?
	var songH1 = songData.getElementsByTagName("h1")[0].innerHTML;
	var songH2 = songData.getElementsByTagName("h2")[0].innerHTML;
	var nowPlaying = document.getElementById("now-playing");
	nowPlaying.getElementsByTagName("h1")[0].innerHTML = songH1;
	nowPlaying.getElementsByTagName("h2")[0].innerHTML = songH2;

	window.requestAnimationFrame(playheadAnim);
}


function selectClickedSong(clickEvent) {
	var newSongData = getContainerLi(clickEvent.target);
	selectSong(newSongData);
}


function playSong() {
	sound.play();
	playButton.style.visibility = "hidden";
	pauseButton.style.visibility = "visible";
}

function pauseSong() {
	sound.pause();
	playButton.style.visibility = "visible";
	pauseButton.style.visibility = "hidden";
}

function nextSong() {
	var wasPlaying = sound.playing();
	var newSongData = activeSong.nextElementSibling;
	if (newSongData) {
		selectSong(newSongData);
		if (wasPlaying) {
			playSong();
		}
		return true;
	}
}

function nextSongPlay() {
	isNextSong = nextSong();
	if (isNextSong) {
		playSong();
	}
}

function prevSong() {
	var wasPlaying = sound.playing();
	var newSongData = activeSong.previousElementSibling;
	if (newSongData) {
		selectSong(newSongData);
		if (wasPlaying) {
			playSong();
		}
	}
}

function playheadAnim() {
	progressAmount = sound.seek() / duration;
	if (!progressAmount) {
		progressAmount = 0;
	}
	setPlayHead(progressAmount);

	if (sound.state() == "loading") {
		loadingIndicator.classList = []
	}
	else {
		loadingIndicator.classList = ["hidden"]	
	}

	if (sound.playing() || sound.state() == "loading") {
		window.requestAnimationFrame(playheadAnim);
	}
	else {
		playButton.style.visibility = "visible";
		pauseButton.style.visibility = "hidden";
	}
}

playButton.onclick = playSong;
pauseButton.onclick = pauseSong;
nextButton.onclick = nextSong;
prevButton.onclick = prevSong;



timeline.addEventListener("click", seekToClick, false);
timelineProgressOverlay.addEventListener("click", seekToOverlayClick, false);

for(var i = 0;i<songs.length;i++){
    songs[i].addEventListener("click", selectClickedSong);
}

selectSong(songs[0]);

window.requestAnimationFrame(playheadAnim);