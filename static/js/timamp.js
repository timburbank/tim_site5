/*
<audio> tag can't seek in chrome, maybe due to missing 
Content-Length, Content-Range, Content-Type headers
(either provided by webserver, or in file metadata?)

Also I think current version awkardly loads all audio files at once

Maybe worth using library like howler.js to handle these edge cases
Or maybe I need to learn to server audio right?
*/


var activeAudio = document.getElementById("active-audio");
var playButton = document.getElementById("play");
var pauseButton = document.getElementById("pause");
var nextButton = document.getElementById("next-button");
var prevButton = document.getElementById("prev-button");
var timeline = document.getElementById("timeline-control");
var timelineProgress = document.getElementById("timeline-progress");
var timelineProgressOverlay = document.getElementById("timeline-progress-overlay");
var timelineUnProgressOverlay = document.getElementById("timeline-unprogress-overlay");
var progressBarFilled = document.getElementById("progress-filled");
var nowPlayingCover = document.getElementById("now-playing-cover");
var songs = document.getElementsByClassName("song");
var duration;




function setPlayHead(progressAmount) {
	/*
	ProgressAmount is a value from 0-1
	*/
	var progressPercent = progressAmount * 100;
	var remainingPercent = (1 - progressAmount) * 100;

	overlayCss = "right:" + remainingPercent + "%;"
	timelineProgressOverlay.style.cssText = overlayCss;

	unProgressOverlayCss = "left:" + progressPercent + "%;"
	timelineUnProgressOverlay.style.cssText = unProgressOverlayCss;	

	progressCss = "clip-path: inset(0 " + remainingPercent + "% 0 0);"
	timelineProgress.style.cssText = progressCss;

	progressBarFilled.style.width = progressPercent + "%";
}


function timeUpdate() {
	var progressAmount = activeAudio.currentTime / activeAudio.duration;
	setPlayHead(progressAmount);
}


function seekToClick(clickEvent) {
	//// offsetX is listed as experimental, but behaves consistently 
	//// between browsers
	var xClickPos = clickEvent.offsetX;
	var timelineWidth = timeline.offsetWidth;
	var targetTime = (xClickPos / timelineWidth) * activeAudio.duration;

	console.log(xClickPos);
	console.log(timelineWidth);
	console.log(targetTime);

	activeAudio.currentTime = targetTime;

	console.log(activeAudio.currentTime);
}

function seekToOverlayClick(clickEvent) {
	/* 
	Timeline overlay is a gradient effect that follows
	the playhead and prevents us from clicking on the timeline.
	*/

	//// offsetX is listed as experimental, but behaves consistently 
	//// between browsers
	var xClickPos = clickEvent.offsetX;
	var timelineWidth = timeline.offsetWidth;
	var clickTimeBeforePlayhead = (1 - (xClickPos / timelineWidth)) * activeAudio.duration;
	var targetTime = activeAudio.currentTime - clickTimeBeforePlayhead;

	activeAudio.currentTime = targetTime;
}


function resetSong(audio) {
	audio.pause();
	audio.currentTime = 0;
	setPlayHead(0);
	playButton.style.visibility = "visible";
	pauseButton.style.visibility = "hidden";
}


function getContainerLi(domObject) {
	if (domObject.tagName == "LI") {
		return domObject;
	}
	else {
		return getContainerLi(domObject.parentElement);
	}
}


function selectSong(songData) {
	/*
	songData is a container (usually li) element with all the song data tags
	*/

	//// we switch which audio object is active instead of replacing
	//// the source of the active one, which is probably silly
	try {
		resetSong(activeAudio);
		activeAudio.removeEventListener("timeupdate", timeUpdate, false);
		activeAudio.removeEventListener("ended", nextSongPlay);
	} catch (error) {
		console.error(error);
	}

	//// Show new song active in song list
	activeAudio.parentElement.setAttribute("class", "song");
	songData.setAttribute("class", "song active");

	activeAudio = songData.getElementsByTagName("audio")[0];

	duration = activeAudio.duration;
	activeAudio.addEventListener("timeupdate", timeUpdate, false);
	activeAudio.addEventListener("ended", nextSongPlay);
	activeAudio.currentTime = 0;


	var coverUrl = songData.getElementsByClassName("cover")[0].src;
	nowPlayingCover.src = coverUrl;


	var timelineUrl = songData.getElementsByClassName("timeline")[0].src;
	timeline.src = timelineUrl;
	timelineProgress.getElementsByTagName("img")[0].src = timelineUrl;

	// most of these are delcared at beginning, but maybe this better?
	var songH1 = songData.getElementsByTagName("h1")[0].innerHTML;
	var songH2 = songData.getElementsByTagName("h2")[0].innerHTML;
	var nowPlaying = document.getElementById("now-playing");
	nowPlaying.getElementsByTagName("h1")[0].innerHTML = songH1;
	nowPlaying.getElementsByTagName("h2")[0].innerHTML = songH2;
}


function selectClickedSong(clickEvent) {
	var newSongData = getContainerLi(clickEvent.target);
	selectSong(newSongData);
}


function playSong() {
	activeAudio.play();
	playButton.style.visibility = "hidden";
	pauseButton.style.visibility = "visible";
}

function pauseSong() {
	activeAudio.pause();
	playButton.style.visibility = "visible";
	pauseButton.style.visibility = "hidden";
}

function nextSong() {
	var wasPlaying = !activeAudio.paused;
	var newSongData = activeAudio.parentElement.nextElementSibling;
	if (newSongData) {
		selectSong(newSongData);
		if (wasPlaying) {
			playSong();
		}
		return true;
	}
}

function nextSongPlay() {
	isNextSong = nextSong();
	if (isNextSong) {
		playSong();
	}
}

function prevSong() {
	var wasPlaying = !activeAudio.paused;
	var newSongData = activeAudio.parentElement.previousElementSibling;
	if (newSongData) {
		selectSong(newSongData);
		if (wasPlaying) {
			playSong();
		}
	}
}


activeAudio.addEventListener("timeupdate", timeUpdate, false);
activeAudio.addEventListener("ended", nextSongPlay);

playButton.onclick = playSong;
pauseButton.onclick = pauseSong;
nextButton.onclick = nextSong;
prevButton.onclick = prevSong;

activeAudio.addEventListener("canplaythrough", function () {
	duration = activeAudio.duration;
}, false);


timeline.addEventListener("click", seekToClick, false);
timelineProgressOverlay.addEventListener("click", seekToOverlayClick, false);

for(var i = 0;i<songs.length;i++){
    songs[i].addEventListener("click", selectClickedSong);
}

selectSong(songs[0]);
