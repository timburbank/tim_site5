Saira Semi Condensed
Omnibus-Type 
https://www.omnibus-type.com/fonts/saira-semi-condensed/
SIL Open Font License, 1.1 (SairaSemiCondensedOFL.txt)

Lato
https://www.latofonts.com/
https://fonts.google.com/specimen/Lato
SIL Open Font License, 1.1 (LatoOFL.txt)

Open Sans
https://www.opensans.com/
Apache License (ApacheLicense.txt)